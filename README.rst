.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


======================
Le Filament - CRM
======================

Hérite du module CRM
   - Ajoute un rapport sur le temps de conversion
   - Ajoute un champ sur le type d'apporteur d'affaire et le partner associé

Credits
=======

Contributors ------------

* Benjamin Rivier <benjamin@le-filament.com>


Maintainer ----------

.. image:: https://le-filament.com/img/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament