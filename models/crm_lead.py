# -*- coding: utf-8 -*-

# © 2017 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import fields, models


class LeFilamentLead(models.Model):
    _inherit = 'crm.lead'

    type_apporteur = fields.Selection([('reseau', 'Réseau'),
                                       ('partenaire', 'Partenaire'),
                                       ('client', 'Client'),
                                       ('perso', 'Personnel'),
                                       ('projet', 'Suite Projet')],
                                      string="Type Apporteur")
    apporteur = fields.Many2one('res.partner', string="Apporteur")
