# -*- coding: utf-8 -*-

# © 2017 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': 'Le Filament - CRM',

    'summary': """
        CRM Le Filament
         - Ajoute un rapport sur le temps de conversion

    """,

    'version': '10.0.1.0',
    'license': 'AGPL-3',
    'description': """

    """,
    'author': 'LE FILAMENT',
    'category': 'Project',
    'depends': ['sale'],
    'contributors': [
        'Benjamin Rivier <benjamin@le-filament.com>',
    ],
    'website': 'http://www.le-filament.com',
    'data': [
        # 'views/assets.xml',
        'views/crm_lead_view.xml',
        'report/crm_conversion_report_view.xml',
    ],
    'qweb': [
        'static/src/xml/*.xml',
    ],
}
